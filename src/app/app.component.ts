import { Component, ViewEncapsulation } from '@angular/core';

/**
 * Component DOC
 */
@Component({
  selector: 'app-pancakes',
  template: `
  <nav-bar></nav-bar>
  <div class="container">
    <div class="row">
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>
  </div>
  `,
  styles: [`
    .h1{
      color: red;
    }
  `]
})
export class AppComponent {

  data = {
    value: 'Superman'
  }

  update(value){
    this.data = Object.assign({}, this.data,{
      value
    })
  }


  title = 'Batman!'; 
  constructor(){ 
    
  }

}
