import { Component, OnInit, Input, Inject, ChangeDetectionStrategy} from '@angular/core';
import {Album, AlbumImage} from './interfaces'

//let count = 0;
// img src=[someDta$]  hostBinding('src')='loader.gif'

@Component({
  selector: '.album-card',
  template: `
  <img class="card-img-top" 
      [src]="image.url"
      [width]="image.width"
      alt="Card image cap">
    
    <div class="card-block">
      <h4 class="card-title">{{album.name | shorten:25 }}</h4>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [`
    :host{
      min-width:30%;
    }
    img{
      width:100%
    }
  `],
  providers:[
    // {provide:'test', useFactory: (MusicServie)=>{
    //   count++
    //   return count;
    // }}
  ]
})
export class AlbumCardComponent implements OnInit {

  @Input('album')
  set onAlbum(album){
    this.album = album;
    this.image = album.images[1];
  }

  album:Album;

  image:AlbumImage;

  //constructor(@Inject('test') test ) { console.log('counter'+test) }
  constructor(){}

  ngOnInit() {
  }

}
