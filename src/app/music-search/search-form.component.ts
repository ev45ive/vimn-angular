//import { FormControlName, FormGroupName } from '@angular/forms/src/directives';
import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service';
import { FormGroup, AbstractControl, FormControl, Validators, FormBuilder, ValidatorFn, AsyncValidatorFn, ValidationErrors } from '@angular/forms'

@Component({
  selector: 'search-form',
  template: `
    <form [formGroup]="queryForm">
      <div class="input-group">
        <input class="form-control" formControlName="query">
      </div>
      
      <div *ngIf="queryForm.pending"> Checking.... Please Wait! </div>

      <ng-container *ngIf="queryField.dirty || queryField.touched">
        <div *ngIf="queryField.errors?.required"> Field is required </div>
        <div *ngIf="queryField.errors?.no_batman"> Field should not be "{{queryField.errors?.no_batman.text}}" </div>
        <div *ngIf="queryField.errors?.minlength"> Too short!
        (minimum {{queryField.errors?.minlength.requiredLength}}))
        </div>
      </ng-container>
    </form>
  `,
  styles: [`
    input.ng-invalid.ng-dirty,
    input.ng-invalid.ng-touched{
      border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm:FormGroup;
  queryField:AbstractControl;

  constructor(private service:MusicService,
              private builder: FormBuilder) {

    const noBatman = (text = ''):ValidatorFn => (control:AbstractControl) => {
      const ok = control.value != text
      
      return !ok? <ValidationErrors>{
        'no_batman': { text: text }
      } : null
    }

    const asyncNoBatman = (text = ''):AsyncValidatorFn => (control:AbstractControl) => {
      const ok = control.value != text
      
      return new Promise( resolve => {
        setTimeout(()=>{
            resolve(!ok? <ValidationErrors>{
            'no_batman': { text: text }
          } : null)
        },2000)
      })
    }

    this.queryForm = builder.group({
        query: builder.control('',[
          Validators.required,
          Validators.minLength(3)
        ],[
          asyncNoBatman('Batman')
        ])
      })
    this.queryField = this.queryForm.get('query')

    // this.queryField = new FormControl('',[
    //       Validators.required,
    //       Validators.minLength(3)
    //    ])

    // this.queryForm = new FormGroup({
    //    'query': this.queryField
    // })

    const status$ = this.queryForm.get('query').statusChanges;
    const value$ = this.queryForm.get('query').valueChanges;

    //.filter( query => this.queryField.valid )
    value$
    .debounceTime(400)
    .distinctUntilChanged()
    .filter( query => query.length > 3 )
    .subscribe( query => {
      this.search(query)
    })

    console.log(this.queryForm)
  }

  search(query){
    this.service.queryAlbums(query)
  }

  ngOnInit() {
  }

}
