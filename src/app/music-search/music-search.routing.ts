import { identifierModuleUrl } from '@angular/compiler/compiler';
import { RouterModule, Route, Routes } from '@angular/router'

import {MusicSearchComponent} from './music-search.component'

const routes:Routes = [
  { path: '', component: MusicSearchComponent  },
]

export const routingModule = RouterModule.forChild(routes)