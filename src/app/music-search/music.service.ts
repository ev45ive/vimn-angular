import { Observable, Subject, ReplaySubject } from 'rxjs/Rx';
import { Injectable, Inject, InjectionToken } from '@angular/core';
import {Http, Response} from '@angular/http'
import {Album} from './interfaces'

export const MUSIC_URL = new InjectionToken('MUSIC_URL')

@Injectable()
export class MusicService {

  albums:Album[] = []

  albums$ = new Subject<Album[]>()
  queries$ = new Subject<string>()

  constructor(
    private http:Http,
    @Inject(MUSIC_URL) private urlFactory
  ){
    this.queries$
    .map(this.urlFactory)
    .switchMap((url:string) => this.http.get(url) )
    //.flatMap((url:string) => Observable.throw('ups...') )
    .map( (resp:Response) => resp.json() )
    .map( data => <Album[]>data.albums.items)   
    .subscribe( albums =>{
      this.albums = albums;
      this.albums$.next(albums)
    },err=>{
      console.log('Error', err)
    })

    this.queryAlbums('metallica')
  }

  getAlbums$(){
    return this.albums$.startWith(this.albums)
  }

  queryAlbums(query = 'metallica'){
    return this.queries$.next(query)
  }
}
