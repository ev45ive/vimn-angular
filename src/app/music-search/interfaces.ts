export interface Entity{
    id:string,
    name: string
}

export interface Album extends Entity {
    artists: Artist[],
    images: AlbumImage[],
}

export interface Artist extends Entity{
}

export interface AlbumImage{
    height: number
    width: number
    url: string
}

// export interface HashMap<T>{
//     [key:string]:Artist
// }