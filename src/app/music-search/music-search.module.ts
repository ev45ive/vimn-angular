import { NgModule, FactoryProvider  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumListComponent } from './album-list.component';
import { AlbumCardComponent } from './album-card.component';
import { MusicService, MUSIC_URL } from './music.service'
import { URLSearchParams } from '@angular/http'
import { routingModule } from './music-search.routing'

import { SharedModule } from '../shared'

export function urlFactory(){
  let Params = new URLSearchParams()
  Params.set('type','album')
  Params.set('market','pl')
  return (query:string):string => {
    Params.set('query', query)
    return `https://api.spotify.com/v1/search?`+Params
  }
}

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    routingModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumListComponent,
    AlbumCardComponent
  ],
  exports:[
    //MusicSearchComponent
  ],
  providers:[
    //{provide: MusicService, useClass: MusicService },
    
    MusicService,

    {provide: MUSIC_URL, useFactory: urlFactory}
  ]
})
export class MusicSearchModule { }
