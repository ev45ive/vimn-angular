import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, Inject, InjectionToken } from '@angular/core';
import { MusicService } from './music.service'
import {Album} from './interfaces'

@Component({
  selector: 'album-list',
  template: `
    <div class="card-group" *ngIf="albums$ | async as albums">
        <div class="card album-card" [album]="album"
        *ngFor="let album of albums trackBy id"></div>
    </div>
  `,
  styles: []
})
export class AlbumListComponent implements OnInit {

  data = {
    value: 1
  }
  //albums:Album[] = []
  //sub: Subscription

  albums$:Observable<any>;

  constructor(
    private musicService:MusicService
  ) {
    this.albums$ = this.musicService.getAlbums$()
    
    // .subscribe( albums => {
    //   this.albums = albums
    // })
   
  }
  // ngOnDestroy(){
  //   this.sub.unsubscribe()
  // }

  ngOnInit() {
  }

}
