import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service'

@Component({
  selector: 'music-search',
  template: `
    <search-form></search-form>
    <album-list></album-list>
  `,
  styles: [],
  providers:[
   // MusicService
  ]
})
export class MusicSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
