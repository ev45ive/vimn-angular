import { RouterModule, Route, Routes } from '@angular/router'
import { CounterComponent } from './counter.component'
const routes:Routes = [
  // Home Page:
  { path: '', redirectTo: 'playlists', pathMatch:'full' },
  { path: 'music', 
    loadChildren: './music-search/music-search.module#MusicSearchModule'
  },
  { path: 'counter', component: CounterComponent }
  // Pages:
  //{ path: 'playlists', component: PlaylistsComponent  },
  // 404 - page not found:
  //{ path: '**', redirectTo: 'music', pathMatch:'full'  },
]

export const routingModule = RouterModule.forRoot(routes, {
  //enableTracing:true,
  //useHash: true
})