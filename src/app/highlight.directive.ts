import { Directive, ElementRef, Input, OnInit, HostListener, HostBinding, HostDecorator, Renderer} from '@angular/core';

@Directive({
  selector: '[highlight]',
  exportAs:'hightlight'
  // host:[
  //   '(mouseenter)':'onmouseenter'
  // ]
})
export class HighlightDirective implements OnInit {

  constructor( //private elem:ElementRef,
            private renderer:Renderer) { 
    //console.log('Hi', this)
  }

  color = 'red';

  @Input('highlight')
  set setColor(color){
    this.color = color;
    this.renderColor()
  }

  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.active? this.color : 'inherit'
  }

  active = false;

  @HostListener('mouseenter', ['$event.target'])
  onmouseenter(elem){
    this.active = true
  }

  @HostListener('mouseleave',['$event'])
  onmouseleave($event){
    this.active = false
  }

  renderColor(){
    // this.renderer.setElementStyle(this.elem.nativeElement,
    // 'borderLeftColor', this.active? this.color : '')
    // this.elem
    //   .nativeElement
    //   .style
    //   .borderLeftColor = this.color;
  }

  ngOnInit(){
    //this.renderColor()
  }

}
