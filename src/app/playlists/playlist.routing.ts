import { ResolvePlaylist } from './playlists.module';
import { Observable } from 'rxjs/Rx';
import { identifierModuleUrl } from '@angular/compiler/compiler';
import { RouterModule, Route, Routes, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'

import { PlaylistsComponent } from './playlists.component'
import { PlaylistDetailComponent } from './playlist-detail.component'
import { PlaylistListComponent } from './playlist-list.component'


const routes: Routes = [
  {
    path: 'playlists', component: PlaylistsComponent,
    children: [
      {
        path: '',
        component: PlaylistDetailComponent,
      },
      {
        path: 'show/:id',
        component: PlaylistDetailComponent, 
        data: { VALUE: 'TEST'},
        
        resolve:{
         // playlist: 'resolvePlaylist'
        }
      },
    ]
  }
]

// { path: 'show/:id', component: PlaylistListComponent, outlet:'sidebar'},
// { path: 'show/:id', component: PlaylistDetailComponent, outlet:'primary'}

export const routingModule = RouterModule.forChild(routes)