import { Playlist } from './interfaces';
import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { PlaylistsComponent } from './playlists.component'
import { PlaylistListComponent } from './playlist-list.component';
import { PlaylistItemComponent } from './playlist-item.component';
import { PlaylistDetailComponent } from './playlist-detail.component';
import { HighlightDirective } from '../highlight.directive';
import { routingModule } from './playlist.routing'
import { PlaylistsService } from './playlists.service'

import { ValidcolorDirective } from '../validcolor.directive'

import { Observable } from 'rxjs'
import { RouterModule, Route, Routes, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'

@Injectable()
export class ResolvePlaylist implements Resolve<Playlist>{

  constructor(private service: PlaylistsService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    return this.service.getPlaylist$(route.params.id)
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routingModule
  ],
  declarations: [
    PlaylistsComponent,
    PlaylistListComponent,
    PlaylistItemComponent,
    PlaylistDetailComponent,
    HighlightDirective,
    ValidcolorDirective
  ],
  providers:[
    PlaylistsService,
    {provide: 'resolvePlaylist', useClass: ResolvePlaylist}
  ]
})
export class PlaylistsModule { }
