export interface Playlist{
    id?: string,
    name: string
    favourite: boolean
    /**
     * Please use HEX color values, thanks! ;-)
     */
    color: string
}