import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Playlist } from './interfaces'
import { Store } from '@ngrx/store'
import { PlaylistsState, selectPlaylists, 
  updatePlaylist, addPlaylist,removePlaylist } from './playlist.reducer'


@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = []

  getPlaylists$(){
    return selectPlaylists(this.store.select<PlaylistsState>('playlists'))
  }

  getPlaylist$(playlist_id = ''){
    this.store.dispatch({
      type:"FETCH_PLAYLISTS"
    })
    return this.store.select<Playlist>('playlists','index',playlist_id)
  }

  savePlaylist(playlist:Playlist){
    if(playlist.id){
      // updates:
      this.store.dispatch({
        type:"SAVE_PLAYLIST",
        payload:{
          playlist
        }
      })
    }else{
      // add new:
      this.store.dispatch(addPlaylist(playlist))
    }
  }

  constructor(private store:Store<PlaylistsState>) { 

  }

}
