import { StoreModule, ActionReducer, Action, combineReducers } from '@ngrx/store'
import { Playlist } from './interfaces'
import { Observable } from 'rxjs'

export interface PlaylistsState {
    index: {
        [id: string]: Playlist
    },
    order: string[]
}

export const initialPlaylistState: PlaylistsState = {
    index: {
        'dsgrr': {
            id: 'dsgrr',
            name: 'Angular hits',
            color: '#ff0000',
            favourite: false
        },
        'sgrbn': {
            id: 'sgrbn',
            name: 'The best of angular',
            color: '#00ff00',
            favourite: true
        },
        'feshh': {
            id: 'feshh',
            name: 'Angular hits',
            color: '#0000ff',
            favourite: false
        }
    },
    order: ['dsgrr', 'sgrbn', 'feshh']
}

//const selector = (projector, ...paths) => {}

export const selectPlaylists = (playlists:Observable<PlaylistsState>) => {
   const index$ = playlists.map( state => state.index ).distinctUntilChanged()
   const order$ = playlists.map( state => state.order ).distinctUntilChanged()

   return index$.combineLatest( order$, (index,order)=>(
       order.map( id => index[id] )
   ))
}

type PlaylistActionType = 'UPDATED_PLAYLIST'
                        | 'ADDED_PLAYLIST'
                        | 'REMOVED_PLAYLIST';

interface UPDATED_PLAYLIST{
    type: 'UPDATED_PLAYLIST',
    payload:{
        playlist:Playlist
    }
}
type ACTIONS = UPDATED_PLAYLIST | Action;

                        
interface PlaylistAction extends Action {
    type: PlaylistActionType
}

export const updatePlaylist = (playlist: Playlist) => {
    if(playlist['name'].length < 3){
        return;
    }
    
    return {
        type: 'UPDATED_PLAYLIST',
        payload: {
            playlist
        }
    }
}
export const addPlaylist = (playlist: Playlist) => ({
    type: 'ADDED_PLAYLIST',
    payload: {
        playlist
    }
})
export const removePlaylist = (id: string) => ({
    type: 'REMOVED_PLAYLIST',
    payload: {
        id
    }
})


export function playlistReducer(state, action:ACTIONS){
    switch(action.type){
        case 'ADDED_PLAYLIST':
            return state  
        case 'SAVED_PLAYLIST':
        case 'UPDATED_PLAYLIST':
            let playlist = action.payload.playlist;
            return {
                ...state,
                index: {...state.index, [playlist.id]: playlist }
            }
        case 'REMOVED_PLAYLIST':
            let id = action.payload.id;
            return {
                ...state
            }  
        case 'LOADED_PLAYLISTS':
            let playlists = action.payload.playlists;
            let index = playlists.reduce((index,playlist)=>{
                index[playlist.id] = playlist;
                return index
            },{})
            let order = playlists.map( playlist => playlist.id )

            console.log(playlists, index, order)

            return {
                ...state,
                index: {...state.index, ...index},
                order: order
            }
        default:
            return state
    }
}