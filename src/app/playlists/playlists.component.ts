import { Component, OnInit } from '@angular/core';
import { Playlist } from './interfaces'

@Component({
  selector: 'playlists',
  template: `
    <div class="row">
      <div class="col">
        <playlist-list (selected)=" selected = $event "></playlist-list>  
        <router-outlet name="sidebar"></router-outlet>
      </div>
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>   
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selected:Playlist = {
    name:'Angular greatest Hits!',
    favourite: true,
    color: '#ff0000'
  }


  constructor() { }

  ngOnInit() {
  }

}
