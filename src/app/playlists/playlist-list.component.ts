import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { Playlist } from './interfaces'
import { PlaylistsService } from './playlists.service'
import { Router, ActivatedRoute } from '@angular/router'

// (mouseenter)="activeOne = playlist"
// (mouseleave)="activeOne = null"
// [style.borderLeftColor]="activeOne == playlist? playlist.color : 'initial' "

// function MyDecorator(){
//   return function<T>(target:T, ket, descr){
//     console.log(target);
//     return target;
//   }
// }


@Component({
  selector: 'playlist-list',
  template: `
    <ul class="list-group">
      <li class="list-group-item item-border-left"
        (click)="selectPlaylist(playlist)"

        [highlight]="playlist.color"        
        [class.active]=" playlist.id == selected_id " 
        *ngFor="let playlist of playlists$ | async; let i = index">
          {{playlist.name}}
      </li>
    </ul>  `,
  encapsulation: ViewEncapsulation.Emulated,
  styles: [`
    .item-border-left{
      border-left:5px solid black;
    }
  `]
})
export class PlaylistListComponent implements OnInit {

  // @Output('selected')
  // selectedEmitter = new EventEmitter<Playlist>()

  selectPlaylist(playlist){
   //console.log(playlist.id)
   this.router.navigate(['/playlists','show', playlist.id ])
   // this.selectedEmitter.emit(this.selected)
  }

  selected_id = '';

  playlists$

  constructor(private router:Router,
              private route: ActivatedRoute,
              private service: PlaylistsService)
  {
    route.firstChild && route.firstChild.params.subscribe( params => {
      //console.log(params, router.routerState)
      
      this.selected_id = params.id
    })
    router.events.subscribe( event => {
      //console.log(event, router.routerState )
    })

    this.playlists$ = this.service.getPlaylists$()
  }

  ngOnInit() {
  }

}