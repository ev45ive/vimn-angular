import { NgForm } from '@angular/forms';
import { Component, OnInit, Input, Inject, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Playlist } from './interfaces'
import { PlaylistsService } from './playlists.service'
import { ActivatedRoute } from '@angular/router'


@Component({
  selector: 'playlist-detail',
  template: `
  <div *ngIf=" (playlist$ |async ) as playlist ">
    <p (click)="edit = !edit">{{ edit? 'Show' : 'Edit'}} </p>


    <div *ngIf="edit; then showForm; else editForm"></div>

    <ng-template #loading>
      Select playlist...
    </ng-template>

    <ng-template #showForm>
      <div>
        <p>{{ playlist.name }}</p>
        <p>{{ playlist.favourite? 'favourite' : '' }}</p>
        <p [style.color]="playlist.color"> {{playlist.color}} </p>
      </div>
    </ng-template>

    <ng-template  #editForm>
      <form #playlistForm="ngForm" (ngSubmit)="playlistForm.valid && save(playlist, playlistForm.value)">
        <div class="input-group">
          <label> Name: </label>
          <input type="text" [ngModel]="playlist.name" #nameRef="ngModel" required name="name">
          <div *ngIf="nameRef.errors?.required"> Field is required </div>
        </div>
        <div class="input-group">
          <label> Favourite: </label>
          <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
        </div>
        <div class="input-group">
          <label> Color: </label>
          <input type="text" [ngModel]="playlist.color" #color="ngModel" name="color" required validcolor>
          <div *ngIf="color.errors?.invalid_color">Invalid color: {{color.errors?.invalid_color}}</div>
        </div>
        <input type="submit" class="btn btn-success" value="Save">
      </form>
    </ng-template>
  </div>
  `,
  providers:[
    // {provide:'item$', useFactory:(service, route)=>{
    //   return  route.params
    //   .map( params => params.id)
    //   .switchMap( id => service.getPlaylist$(id))
    // }, deps:[PlaylistsService, ActivatedRoute]}
  ],
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ]
})
export class PlaylistDetailComponent implements OnInit {

  save(oldPlaylist, newPlaylist){
    let playlist = {...oldPlaylist, ...newPlaylist}
    this.service.savePlaylist(playlist)
  }

  // @ViewChild(NgForm)
  // playlistForm

  @ViewChildren(NgForm)
  playlistForm = new QueryList<NgForm>()

  // @Input('playlist')
  // playlist:Playlist;

  playlist$;

  //ngAfterViewChecked(){
  //  
  //}
  ngAfterViewInit(){
    this.playlistForm.changes.subscribe( form => {
      console.log('test', form )
    })
  }

  constructor(//@Inject('item$') private playlist$, 
              private route: ActivatedRoute,
              private service: PlaylistsService) {

    // this.playlistForm.changes.subscribe( changes => {
    //   console.log(changes)
    // })


    //let id = route.snapshot.params['id']
    // route.data.subscribe(_=>{
    //   //console.log(_)
    //   this.playlist$ = _.playlist
    // })
    
    this.playlist$ = route.params
    .map( params => params.id)
    .switchMap( id => service.getPlaylist$(id))

    // .subscribe( id =>{
    //   this.playlist$ = service.getPlaylist$(id)
    // })
  }

  ngOnInit() {

  }

}
