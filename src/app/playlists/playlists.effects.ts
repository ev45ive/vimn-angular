import { Playlist } from './interfaces';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class PlaylistEffects{
    constructor(
        private http: Http,
        private actions$: Actions
    ){
        actions$.subscribe(_=>{
            console.log(_)
        })
    }


    @Effect()
    otherEffect$ = new Subject()

    
    @Effect()
    savePlaylist$ = this.actions$
    .ofType('SAVE_PLAYLIST')
    .switchMap(action =>{
        let playlist = action.payload.playlist;
        return this.http.put('http://localhost:3000/playlists/'+playlist.id,playlist)
    })
    .map( resp => resp.json())
    .map( data => {
        return {
            type:"SAVED_PLAYLIST",
            payload:{
                playlist: data
            }
        }
    })

    @Effect()
    fetchPlaylists$ = this.actions$
    .ofType('FETCH_PLAYLISTS')
    .map(()=>'http://localhost:3000/playlists')
    .switchMap(url => this.http.get(url))
    .map( resp => resp.json())
    .map( data => {
        return {
            type:"LOADED_PLAYLISTS",
            payload:{
                playlists: data
            }
        }
    })
}