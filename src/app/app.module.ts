import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import './operators';

import { routingModule } from './app.routing'
import { AppComponent } from './app.component';
import { UnlessDirective, WhateverComponent } from './unless.directive';
import { LifeCycleComponent } from './life-cycle.component';
//import { MusicSearchModule } from './music-search/music-search.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { NavBarComponent } from './nav-bar.component';
import { reducer, initialState } from './app.store';
import { CounterComponent } from './counter.component'
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, combineReducers } from '@ngrx/store';
import { PlaylistEffects } from './playlists/playlists.effects'


@NgModule({
  declarations: [
    AppComponent,
    UnlessDirective,
    WhateverComponent,
    LifeCycleComponent,
    NavBarComponent,
    CounterComponent
  ],
  entryComponents:[
    WhateverComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routingModule,
    StoreModule.provideStore(reducer, initialState),
    EffectsModule.run(PlaylistEffects),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
   // MusicSearchModule,
    PlaylistsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }