import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'
import { AppState,decrement,increment } from './app.store'

@Component({
  selector: 'counter',
  template: `
    <p>
      counter Works!
    </p>
    Counter: {{ counter$ | async }}
    <button (click)="inc()"> + </button>
    <button (click)="dec()"> - </button>
  `,
  styles: []
})
export class CounterComponent implements OnInit {

  counter$;

  constructor(private store:Store<AppState>) {  }

  ngOnInit() {    
    this.counter$ = this.store.select<number>('data','counter')
  }

  inc(){
    this.store.dispatch(increment())
  }

  dec(){
    this.store.dispatch(decrement())
  }


}
