import { Directive, Input } from '@angular/core';
import { ValidatorFn, AbstractControl, NG_VALIDATORS, NgModel, NgForm, NG_VALUE_ACCESSOR} from '@angular/forms'

export function validateColor(control:AbstractControl){
  //console.log(control)
  return /^\#/.test(control.value)? null : {'invalid_color':control.value}
}

@Directive({
  selector: '[validcolor]',
  providers:[
    {provide: NG_VALIDATORS ,useValue: validateColor, multi: true }
  ]
})
export class ValidcolorDirective {

  @Input('validcolor')
  validcolor

  ngOnInit(){
    //console.log(this.validcolor)
    //console.log(this.model)
  }

  constructor(private model:NgForm) {
  }

}
