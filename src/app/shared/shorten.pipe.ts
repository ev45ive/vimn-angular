import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  pure: true
})
export class ShortenPipe implements PipeTransform {

  //value = [ Math.random()]

  constructor(){
    // setInterval(()=>{
    //   this.value = [ Math.random()]
    // },100)
  }

  transform(value: any, length = 10): any {
    return value.length >= length ? (value.substr(0, length) + '...') : value;
    //return this.value
  }
}
