import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nav-bar',
  template: `
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      <div class="container">
        <a class="navbar-brand" href="#">Music App</a>
         <div class="collapse navbar-collapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" routerLink="/music">Search Music</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" routerLink="/playlists">Playlist</a>
            </li>
          </ul>
          </div>
        </div>
    </nav>
  `,
  styles: []
})
export class NavBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
