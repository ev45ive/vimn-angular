import { Directive, Component, Input, Output, EventEmitter, TemplateRef, ViewContainerRef, ViewRef, ComponentFactoryResolver} from '@angular/core';

@Component({
  selector:'whatever',
  template: `<div>whatever</div>`,
  inputs:[
    'data:data'
  ]
})
export class WhateverComponent{
  @Output('events')
  events = new EventEmitter()

  constructor(){
    // setInterval(()=>{
    //   this.events.emit( Date.now() )
    // },1000)
  }

}

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  cache: ViewRef

  @Input('unless')
  set onUnlessChange(hide){
    if(hide){
      this.cache = this.vcr.detach()
    }else if(this.cache){
      this.vcr.insert(this.cache)
    }else{
      this.vcr.createEmbeddedView(this.tpl)
      this.cache = this.vcr.get(0)
    }
  }

  @Input()
  data;

  constructor(
    private tpl: TemplateRef<any>, 
    private vcr: ViewContainerRef,
    private resolver:ComponentFactoryResolver
  ) {
    
    // let resolved = this.resolver.resolveComponentFactory(WhateverComponent)
    // resolved.inputs.forEach( x => console.log(x) )
  
    // let componentRef = this.vcr.createComponent(resolved)
    // componentRef.instance['data'] = 'asd';
    // componentRef.instance.events.subscribe(_=>{
    //   console.log('event: ',_)
    // })


  }

}
