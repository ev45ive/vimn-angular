import { RootRenderer } from '@angular/core/core';
import { rootRenderNodes } from '@angular/core/src/view';
import { compose } from '@ngrx/core'
import { StoreModule, ActionReducer, Action, combineReducers } from '@ngrx/store'

import { PlaylistsState, playlistReducer, initialPlaylistState} from './playlists/playlist.reducer'

export interface AppState {    
    counter: number
    playlists: PlaylistsState
}
export const initialState: AppState = {
    counter: 0,
    playlists: initialPlaylistState
}
type ActionType = 'TEST_ACTION'
    | 'INCREMENT'
    | 'DECREMENT';

interface AppAction extends Action {
    type: ActionType
}

export const increment = (): AppAction => ({
    type: 'INCREMENT'
})
export const decrement = (): AppAction => ({
    type: 'DECREMENT'
})

export function counterReducer(state: number = 0, action: AppAction) {
    switch (<ActionType>action.type) {
        case 'INCREMENT': {
            return state + 1
        }
        case 'DECREMENT': {
            return state - 1
        }
        default:
            return state
    }
}

export const reducers = {
    counter: counterReducer,
    playlists: playlistReducer
}

export function reducer(state, action){
    return combineReducers(reducers)(state,action)
}

// export function rootReducer(reducers){
//     return combineReducers(reducers)
// }


// const rootReducer: ActionReducer<AppState> = (state: AppState, action: AppAction) => {
//     switch (<ActionType>action.type) {
//         case 'TEST_ACTION':
//             return state;
//         default:
//             return {
//                 ...state,
//                 data:{
//                     ...state.data,
//                     counter: counterReducer(state.data.counter, action)
//                 }
//             }
//     };
// }


//   var obj = {
//       INCREMENT(){
//         return { ...state, counter: state.counter + 1 }
//       },
//       DECREMENT(){
//         return { ...state, counter: state.counter + 1 }
//       }
//   }