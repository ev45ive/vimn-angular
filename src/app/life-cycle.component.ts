import { Component, Input,
  OnInit, 
  DoCheck, 
  OnChanges, 
  AfterContentChecked, 
  AfterContentInit, 
  AfterViewChecked, 
  AfterViewInit, 
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ApplicationRef
} from '@angular/core';

@Component({
  selector: 'life-cycle',
  template: `
    <p>
      life-cycle, data: {{data.value}} 
    </p>
    <input [(ngModel)]="data.value">
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styles: []
})
export class LifeCycleComponent implements   OnInit, 
  DoCheck, 
  OnChanges, 
  AfterContentChecked, 
  AfterContentInit, 
  AfterViewChecked, 
  AfterViewInit, 
  OnDestroy
 {

  @Input()
  data

  constructor(private cdr: ChangeDetectorRef,
  app: ApplicationRef) {
    console.log(app)
    cdr.detach() 
    // setInterval(()=>{
    // },2000)

    console.log('constructor', arguments)
  }

  ngOnInit() {
    console.log('ngOnInit', arguments)
  }

  ngDoCheck(){
    console.log('ngDoCheck', arguments)
    console.log(this.data)
  } 
  ngOnChanges(){
    console.log('ngOnChanges', arguments)
    this.cdr.detectChanges()
  } 
  ngAfterContentChecked(){
    console.log('ngAfterContentChecked', arguments)
  } 
  ngAfterContentInit(){
    console.log('ngAfterContentInit', arguments)
  } 
  ngAfterViewChecked(){
    console.log('ngAfterViewChecked', arguments)
  } 
  ngAfterViewInit(){
    console.log('ngAfterViewInit', arguments)
  } 
  ngOnDestroy(){
    console.log('ngOnDestroy', arguments)
  }

}
