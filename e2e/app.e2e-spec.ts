import { VimnPage } from './app.po';

describe('vimn App', () => {
  let page: VimnPage;

  beforeEach(() => {
    page = new VimnPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
